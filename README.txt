# bash_script_monitoring.sh library README file
# Copyright (c) 2016 Santeramo Luc (l.santeramo@brgm.fr)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Bureau de Recherches Geologiques et Minieres (BRGM), hereby disclaims all copyright
# interest in the script  "bash script monitoring" written by Luc Santeramo.
#
# v1.0 : Luc Santeramo - 2016-05-03


******** What is it for ? ********

"bash_script_monitoring.sh" is a library designed to monitor (all?) your bash script execution (tested on GNU/Linux et Solaris) 
with your monitoring tool (work only with Zabbix for the moment, but is ready to be adapted for other tools, or just email sending).


******** What's inside ? *********

It's "just" a script using the power of "trap ERR"


******** How to use it ? *********

Use is very simple since you don't need to rewrite all your scripts : this could be sum up as "add 2 lines at the top of your script, and 1 at the bottom !"

2 examples scripts are provided, but precisely you just need to (considering your monitoring environment is ready):
- First time on the monitored host :
  - Copy library "bash_script_monitoring.sh" in directory /usr/share/bash_script_monitoring/
  - Create configuration file /etc/bash_script_monitoring/bash_script_monitoring.conf using provided exemple configuration file
  - Create dir /var/log/bash_script_monitoring to store automatically generated error logs
  - Install zabbix_sender binary (apt-get install zabbix-agent ; yum install zabbix-sender)
  - Properly set "ServerActive" and "Hostname" directives in zabbix_agentd.conf (directory /etc/zabbix)
  - test zabbix_sender configuration after creating a temporary item  for the host (e.g. name "test_item", key "test_key", type "Zabbix trapper", info type "unsigned num") : 
	# zabbix_sender -c /etc/zabbix/zabbix_agentd.conf -k test_key -o 0
    Command line must return : 
	info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000033"
	sent: 1; skipped: 0; total: 1

- On every script you need to monitor :
  - Add following line at the top of the script to be monitored
  - Adapt SCRIPTDIR variable (4th line) if needed, and -s/-c selector (5th line) : "-s" to stop on error, "-C CPT_ERR" to continue on error 

################### Error management environnement ###########################################
# Dont forget to set "-s" or "-c CPT_ERR" on error_check_trap below
# See definition of "error_check_trap" in bash_script_monitoring.sh for parameters description 
SCRIPTDIR="/usr/share/bash_script_monitoring" ; source ${SCRIPTDIR}/bash_script_monitoring.sh
trap 'error_check_trap -s -p $_' ERR ; set -o errtrace ; export SCRIPT_PARAMS="$*";CPT_ERR=0
################### End of error management environnement ####################################

  - Add following lines to the bottom of the script to be monitored

################### Error management environnement ###########################################
# Script execution without error -> monitoring tool is notified
[[ $CPT_ERR -eq 0 ]] && ${execution_status_report_ok}
################### End of error management environnement ####################################

Specific for zabbix monitoring :
  - Add in a model (or directly to the host) a new item with the key user.-path-to-script.script_name.scriptparams, type "Zabbix trapper", info type "unsigned num"
  - Add a trigger when this new item raise a problem (=1 by default)
  - Recommended : on the trigger add a "nodata" check to be sure your script send return values to zabbix

Common for all monitoring tools :
  - Run your script with "bash -vx" the first time, so you will see integration problem or the key generated for zabbix
  - And it's done
  - Recommended : always use bash -vx to run your script and redirect the output to a log file. It will be easier to understand execution errors when it happens


******** What to expect ? ********

- When a command in your script fails with nonzero return value (outside an "if",|| or &&), the script stops or continues, regarding selected behavior (-s or -c), and the monitoring tool is informed (item change to 1 in zabbix).
You can find more information about the error (timestamp, parameters of script and failing command, error line) in /var/log/bash_script_monitoring.
- If all commands in the script run ok, the monitoring tools is informed at the end of the execution (item changed to 0 in zabbix)

During script execution, if you need to alert the monitoring tool for something that is not already handled automatically, 
simply run ${execution_status_report_ok} or ${execution_status_report_nok}, without argument.


******** Thanks to ********

- Nicolas Losantos (BRGM) for helping me on zabbix integration part
- Thomas Calatayud (Departement de Maine-et-Loire) for testing in a different environment
